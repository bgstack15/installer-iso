# This Makefile fragment defines how to pack the pieces of an ISO
# into an actual ISO image.
# env ISO
# env ISOTREE
# env DISTNAME
# env DISTVERSION
# env ARCH
# env BUILD_DATE

VOLI = Devuan ${DISTVERSION} # ${BUILD_DATE}
APPI = Devuan ${DISTNAME} ${DISTVERSION}

${warning ${VOLI} = ${APPI}}

ISOCONTENT = pool.${POOL} ${BOOTOPTIONS} ${FIRMWARE} ${DOCS} ${META} ${MD5SUM}

META += ${ISOTREE}/.disk/info

ifneq (${IS_INSTALLER},)
DOCS = ${INSTALLGUIDE}
META += ${ISOTREE}/.disk/base_installable
META += ${ISOTREE}/.disk/cd_type
META += ${ISOTREE}/debian
META +=${ISOTREE}/devuan

${ISOTREE}/.disk/cd_type:
	echo ${IS_INSTALLER} > $@
endif

MD5SUM = ${ISOTREE}/md5sum.txt
MD5SUM = MD5TMP

# Making the .disk/info file
DISKINFO = Devuan GNU/Linux ${DISTVERSION}
DISKINFO += (${DISTNAME}) ${ARCH}
DISKINFO += - ${ISO} $(BUILD_DATE)
${ISOTREE}/.disk/info: ${ISOTREE}/.disk/
	echo -n "${DISKINFO}" > $@

# Making the .disk/base_installable file
${ISOTREE}/.disk/base_installable: ${ISOTREE}/.disk/
	touch $@

# Making the debian and devuan links
${ISOTREE}/debian ${ISOTREE}/devuan: ${ISOTREE}/.disk/
	ln -s . $@

# Making the disk's md5sum
MD5SUM = ${ISOTREE}/md5sum.txt
MD5TMP = MD5TMP
MD5CMD = (cd ${ISOTREE} ; (find . -type f -printf "%P\0" | xargs -0 md5sum))
.INTERMEDIATE: ${MD5TMP}
${MD5TMP}:
	${MD5CMD} > $@

${MD5SUM}: ${MD5TMP}
	sed '/^isolinux.bin$$/d' ${MD5TMP} > $@

# Making installer documentation by copying from "docs/"
INSTALLGUIDESRC := ${shell find docs -type f | grep -v .gitkeep}
INSTALLGUIDE = ${patsubst docs/%,${ISOTREE}/%,${INSTALLGUIDESRC}}
${INSTALLGUIDE}: ${ISOTREE}/%: docs/%
	mkdir -p $$(dirname $@)
	cp $< $@

# Make an ISO from a populated ${ISOTREE}
XORRISO = xorriso -as mkisofs -r -J -no-emul-boot
XORRISO += -A "${APPI}" -V "${VOLI}" -volset "${ISO}"
XORRISO += -publisher "${DISTPUBLISHER}" -p "${DISTPUBLISHER}"
ifneq (${BOOTBIN},)
XORRISO += -b ${BOOTBIN} -c boot.cat -boot-load-size 4 -boot-info-table
endif
ifneq (${EFIIMG},)
XORRISO += -eltorito-alt-boot --efi-boot ${EFIIMG}
endif

${ISO}-${ARCH}.iso: ${ISOCONTENT}
	${XORRISO} -o $@ ${ISOTREE}
ifneq (${EFIIMG},)
	isohybrid -h 64 -s 32 -u --uefi $@
endif
	#add-firmware-partition $@

# Implements the "build" target
PHONY: build
build: ${ISO}-${ARCH}.iso save-run
