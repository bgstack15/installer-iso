# Makefile fragment for saving details from the last run for posteriority

TIMESTAMP := ${shell date +%Y%m%d%H%M%S}
SAVERUNDIR = content/${DISTNAME}-${DISTVERSION}-${ARCH}/${ISO}-${TIMESTAMP}

${warning SAVERUNDIR = ${SAVERUNDIR}}

SAVERUNPATTERN = udebs-
SAVERUNPATTERN += pool/*_LIST_${ARCH} pool/*_binary-${ARCH}_Packages

.PHONY: save-run
save-run: FILES = udebs-*
save-run: FILES += pool/*_LIST_${ARCH} pool/*_binary-${ARCH}_Packages
save-run: ${SAVERUNDIR}/
	-mv ${FILES} $<
	ls -l $<
