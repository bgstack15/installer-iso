#!/usr/bin/env newlisp

; This script processes a repository Packages file into a directory
; with one file for every package declaration, named by the package.

(signal 2 (fn (x) (exit 0)))

(println (main-args))
(setf DIR (main-args -2) INPUT (main-args -1) COUNT 0
      PREFIX (string "\rCreating " DIR)
      )

(when (file? DIR)
  (write-line 2 (string "** Warning: changing existing directory"))
  )

(unless (file? INPUT)
  (write-line 2 (string "** Abort: cannot read package file"))
  (exit 1))

(print PREFIX )
(make-dir DIR)

(define (version P)
  (or (find-all "([0-9]+)" P (int $1 0 10) 0) 0))

(define (higher-version x y)
  (if (null? x) (null? y) (null? y) true
      (= (x 0) (y 0)) (higher-version (1 x) (1 y))
      (> (x 0) (y 0))))

(define (version-order x y)
  (higher-version (version x) (version y)))

(define (read-version P)
  (if (file? P) ((exec (string "grep ^Version: " P)) 0) ""))

(define (keep-existing P V)
  ;(write-line 2 (string (list "keep-existing" P V)))
  (and (regex "Version: (.*)" (read-version P) 0) (version-order $1 V)))

(dolist (B (parse (read-file INPUT) "\n\n"))
  (print PREFIX " count " (inc COUNT))
  (when (find "^Package: (.*)" B 0)
    (let ((P (format "%s/%s" DIR $1)))
      (unless (keep-existing P (and (find "\nVersion: (.*)" B 0) $1))
        (write-file P (extend B "\n\n"))))))

(println)
(exit 0)
