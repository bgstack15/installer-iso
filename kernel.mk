# Makefile fragment for preparing a an initrd at ${ISOTREE}${INITRD}
# from a root filesystem unpackaging into in ${INITRD_BUILDDIR}

INITRD_BUILDDIR = build/${ARCH}/initrd

${warning INITRD_BUILDDIR = ${INITRD_BUILDDIR}}

# The initrd building must be done by root or fakeroot
INITRDVARS = BUILD_DATE='${BUILD_DATE}' PATH='${PATH}'
INITRDVARS += INITRD='${INITRD}' INITRD_BUILDDIR='${INITRD_BUILDDIR}'
INITRDVARS += ISO='${ISO}' ISOTREE='${ISOTREE}'
INITRDVARS += KERNELVERSION='${KERNELVERSION}' ARCH='${ARCH}'
INITRDVARS += UDEBS_DOWNLOAD='build/${ARCH}/udebs'
INITRDVARS += DISTNAME='${DISTNAME}' DISTVERSION='${DISTVERSION}'

${ISOTREE}${INITRD}: ${INITRD_BUILDDIR}/
	env ${INITRDVARS} /usr/bin/fakeroot make-initrd ${UDEBLIST}

${INITRD_BUILDDIR}/boot/vmlinuz: ${ISOTREE}${INITRD}

${ISOTREE}${KERNEL}: ${INITRD_BUILDDIR}/boot/vmlinuz 
	cp $< $@
