Devuan Installer ISO
====================

This project is tailored for build the Devuan beowulf ISOs, which
includes architectures i386 and amd4 with a collection of ISO for
each:
* netinstall - installation fully over the network
* server - CD size ISO for server installation, also without network
* desktop - 4G ISO for desktop installation also without network
* cd2 - package pool desktop add-on for server install, with xfce4 and 
* cd3 - package pool desktop add-on for server install, with ...

The `pool` Directory
--------------------

This directory contains "seed" files and a Makefile for creating the
package lists for the various ISOs. Use "make -C pool reallyclean" to
clean it up.

How to Build an ISO
-------------------

ISOs are only built for the current (aka host) architecture. Use

    $ make $ISO

with ISO being one of netinstall, server, desktop, cd2 or cd3.
