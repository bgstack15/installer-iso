# Makefile fragment for preparing an EFI boot into build/${ARCH}/efi
# and installing into
# a FAT disk image = ${ISOTREE}/boot/efi.img
# a GRUB boot config = ${ISOTREE}/boot/grub/${ARCH}/grub.cfg
#
# Note: EFI boot set up is the same for all ISO's within an architecture

# Relative path for the boot loader, used by xorriso
EFIGRUB = /boot/grub
EGTHEME = beowulf.theme
EFIGRUBS = grub.cfg font.pf2 efi.img themes/${EGTHEME}

EFIIMG = ${EFIGRUB}/efi.img

${warning ${ISOTREE}${EFIGRUB}/}

# Define a work area for boot partition packaging
ifeq (${ARCH},i386)
FMT = i386-efi
EFI = bootia32.efi
else ifeq (${ARCH},amd64)
FMT = x86_64-efi
EFI = bootx64.efi
else
$(error "unknown EFI architecture ${ARCH}")
endif

# Temporary directory for packaging the boot loader config file
WORK = build/${ARCH}/efi

# Path for the boot loader configuration file, which must be named "grub.cfg"
EFILOAD = /boot/grub
EFILOADCFG = ${WORK}${EFILOAD}/grub.cfg
EFILOADTAR = ${WORK}/grubloader.tar
EFILOADBIN = ${WORK}/grubloader.bin

# Prepare the efi grub config which is included in the efi boot bin-file
#.INTERMEDIATE: ${EFILOADCFG} | ${dir ${EFILOADCFG}}
${EFILOADCFG}: ${dir ${EFILOADCFG}}
	echo 'search --file --set=root /.disk/info' > $@
	echo 'set prefix=($$root)${EFIGRUB}' >> $@
	echo 'source $$prefix/${FMT}/grub.cfg' >> $@

#.INTERMEDIATE: ${EFILOADTAR}
${EFILOADTAR}: ROOT = ${word 1,${subst /, ,${EFILOAD}}}
${EFILOADTAR}: ${EFILOADCFG} | ${dir ${EFILOADTAR}}
	tar cf $@ -C ${WORK} ${ROOT}

# Prepare the grub boot loader in work area
#.INTERMEDIATE: ${EFILOADBIN}
${EFILOADBIN}: ${EFILOADTAR} | ${dir ${EFILOADBIN}}
	grub-mkimage -O ${FMT} -m $< -o $@ -p '(memdisk)${EFILOAD}' \
	  search iso9660 configfile normal memdisk tar part_msdos part_gpt fat

# Compute FAT size from grubby master rounded up as the size of a
# number of 32k blocks
GRUBBINSZ = $(shell stat -c %s "${EFILOADBIN}")
FATSZ = $(shell echo $$(( ( ( $(GRUBBINSZ) / 32768 + 2 ) * 32) )) )

# Pick grub font
${ISOTREE}${EFIGRUB}/font.pf2: /usr/share/grub/ascii.pf2
	@cp $< $@

# Pack the boot loader into a FAT partition in the work area
${ISOTREE}${EFIGRUB}/efi.img: ${EFILOADBIN} | ${ISOTREE}${EFIGRUB}/
	/sbin/mkfs.msdos -v -C "$@" ${FATSZ}
	mmd -i "$@" ::efi
	mmd -i "$@" ::efi/boot
	mcopy -i "$@" "${EFILOADBIN}" "::efi/boot/${EFI}"

# Generic rules for populating target ${EFIGRUB} from templates
${ISOTREE}${EFIGRUB}/themes/${EGTHEME}: | ${ISOTREE}${EFIGRUB}/themes/
${ISOTREE}${EFIGRUB}/themes/${EGTHEME}: boot/grub/${EGTHEME}.tmpl
	env KERNEL=${KERNEL} INITRD=${INITRD} VIDEO_MODE="vga=788 nomodeset" \
	    envsubst '$$KERNEL $$INITRD $$VIDEO_MODE' < $< > $@

${ISOTREE}${EFIGRUB}/%: boot/grub/%.tmpl | ${ISOTREE}${EFIGRUB}/
	env KERNEL=${KERNEL} INITRD=${INITRD} VIDEO_MODE="vga=788 nomodeset" \
	    envsubst '$$KERNEL $$INITRD $$VIDEO_MODE' < $< > $@

# Handle grub modules (.mod and .lst files)
GRUBMODSRCDIR = /usr/lib/grub/${FMT}
GRUBMODDSTDIR = ${ISOTREE}${EFIGRUB}/${FMT}

GRUBMOD_LOAD = ${patsubst %.mod,%,${filter part_%.mod,${GRUBMODS}}}

# grub module files to install
GRUBMODS =  ${sort ${notdir ${wildcard ${GRUBMODSRCDIR}/*.mod}}}
GRUBMODSKIP = configfile fshelp iso9660 memdisk
GRUBMODSKIP += search search_fs_file search_fs_uuid search_label tar
GRUBMODSKIP += affs afs afs_be befs befs_be minix nilfs2 sfs zfs zfsinfo
GRUBMODSKIP += example_functional_test functional_test hello
GRUBMOD_INSTALL = ${filter-out ${addsuffix .mod,${GRUBMODSKIP}},${GRUBMODS}}
GRUBMOD_INSTALL += ${sort ${notdir ${wildcard ${GRUBMODSRCDIR}/*.lst}}}

# install a grub .lst file
${GRUBMODDSTDIR}/%.lst: ${GRUBMODSRCDIR}/%.lst | ${GRUBMODDSTDIR}/
	@cp $< $@

# install a grub .mod file
${GRUBMODDSTDIR}/%.mod: ${GRUBMODSRCDIR}/%.mod | ${GRUBMODDSTDIR}/
	@cp $< $@

# prepare a grub.cfg file for module dir
${GRUBMODDSTDIR}/grub.cfg: ${GRUBMODDSTDIR}/
	for i in ${GRUBMOD_LOAD} ; do echo "insmod $${i%.mod}" ; done > $@
	echo 'source $$prefix/grub.cfg' >> $@

# EFI boot with grub needs a FAT and grub modules in the output tree
.PHONY: boot-efi-grub
boot-efi-grub: ${addprefix ${ISOTREE}${EFIGRUB}/,${EFIGRUBS}}
boot-efi-grub: ${addprefix ${GRUBMODDSTDIR}/,${GRUBMOD_INSTALL}}
boot-efi-grub: ${GRUBMODDSTDIR}/grub.cfg
boot-efi-grub: ${ISOTREE}${INITRD} ${ISOTREE}${KERNEL}

# Install the boot option
BOOTOPTIONS += boot-efi-grub
