#!/bin/zsh
#
# Process stdin package wrt a given package list, and comment out any that
# occurs in the given list.

ls -l $* >&2

typeset -A EXCL
grep -hv '#' $* | while read -r x ; do
    EXCL["$x"]="drop"
done

while read -r x ; do
    if [ -z "$EXCL["$x"]" ] ; then
	echo "$x"
    else
	echo "#$x"
    fi
done

true
