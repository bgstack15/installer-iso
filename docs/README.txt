
                   Devuan GNU/Linux 3.0.0 "beowulf" *

                     (HTML version in README.html)
                
            Welcome to the reliable world of Devuan GNU/Linux

   This disc is one of several installation options for the Devuan 
   GNU+Linux [0] distribution. Devuan offers an extensive collection of 
   original and modified Debian as well as Devuan-specific packages. It 
   is a complete Operating System (OS) for your computer. And it is free  
   as in 'freedom'.

   CONTENTS:
     * Introduction
     * Disc Choices
     * Installation
     * Getting Software
     * Devuan Derivatives
     * Report a Bug
     * Devuan Resources
     
   [0]: https://files.devuan.org/devuan_beowulf/

   Introduction
   ============

   An Operating System (OS) is a set of programs that provide an interface 
   to the computer's hardware. Resource, device, power, and memory management 
   belong to the OS. The core of the OS in charge of operating the circuitry 
   and managing peripherals is called the kernel. The default flavor of Devuan 
   comes with Linux. Most of the basic operating system tools come from 
   the GNU project; hence the name GNU+Linux.
   
   Devuan is available for various kinds of computers, like PC compatible 
   hardware (i386 and amd64) and ARM targets (armel, armhf, arm64) such as 
   the allwinner flavour of SBCs and some ASUS chromebooks.

   Disc Choices
   ============
     * netinstall
     * server (4 CD set)
     * desktop   
     * desktop-live
     * minimal-live

   Proprietary firmware is automatically installed on systems if needed.  
   To avoid installation of non-free firmware, you must choose one of the 
   Expert install options and you must also select a mirror.

   Installation
   ============

   Before you start, please read the Beowulf 3.0.0 Release Notes [0].

   You can install Devuan GNU+Linux either as a dual (or multiple) boot 
   alongside your current OS or as the only OS on your computer.

   An Installation Guide is included on this disc (English version) 
   docs/install-devuan.html and also online (some translations available)  
   https://devuan.org/os/documentation/dev1fanboy 

   You can start the installation program easily by booting your computer 
   with CD/DVD or from USB. Note that some very old or very new systems 
   may not support this.

   [0]: Release_notes_beowulf_3.0.0.txt

   Getting Additional Software
   ===========================

   After installing or upgrading, Devuan's packaging system can use CDs, 
   DVDs, local collections or networked servers (FTP, HTTP) to automatically 
   install software from .deb packages. This is done preferably with the 
   'apt' or 'aptitude'programs.

   You can install packages from the commandline using apt-get. For example, 
   if you want to install the packages 'openssh-client' and 'xlennart', you 
   can give the command:

       apt-get install openssh-client xlennart

   Note that you don't have to enter the complete path or the '.deb' 
   extension. Apt will figure this out itself.

   Or use aptitude for a full screen interactive selection of available 
   Devuan packages.

   Software can also be installed using the Synaptic graphical interface.

   Devuan Derivatives
   ==================

   The default desktop provided by classic installer-iso images shouldn't 
   be considered the only way to use Devuan on the desktop. A growing number 
   of derivative distributions have already adopted Devuan as a base OS. When 
   considering Devuan, we do recommend taking derivatives into consideration. 
   They harness the power of our base distribution by targeting specific usage. 
   This is exactly what we mean to achieve with Devuan. This list is in order 
   of chronological appearance:

     * Gnuinos - https://gnuinos.org
     * Refracta - https://refracta.org
     * Exe GNU/Linux - https://sourceforge.net/projects/exegnulinux/
     * Nelum-dev1 - https://sourceforge.net/projects/nelum-dev1
     * EterTICs - https://gnuetertics.org
     * MIYO - https://sourceforge.net/projects/miyolinux/
     * Star - https://sourceforge.net/projects/linnix
     * heads - https://heads.dyne.org
     * Dowse - https://dowse.eu/
     * good-life-linux - https://sourceforge.net/projects/good-life-linux/
     * Crowz - https://sourceforge.net/projects/crowz/
     * Maemo Leste - https://maemo-leste.github.io/ (in development)

   You are free to create and re-distribute CDs/DVDs of the Devuan 
   GNU+Linux Operating System as well as respins like these.

   Report a Bug
   ============

   This is an official release of the Devuan system. Please report any 
   bugs you find to the Devuan Bug Tracking System at https://bugs.devuan.org.

   If you're reporting bugs against this disc or the installation system, 
   please also mention the version of this disc; this can be found in 
   the file /.disk/info.

   Devuan Resources
   ================

   Learn more about Devuan, Linux and Libre Software

     * The Devuan homepage [1]
     * The Dev1Galaxy web forum [2]
     * Community Communication Channels [3]
     * Twitter @DevuanOrg [5]
     * The Linux Documentation Project [6]
     * General Linux homepage [7]

   [1]: https://devuan.org/
   [2]: https://dev1galaxy.org
   [3]: https://devuan.org/os/community
   [4]: https://twitter.com/DevuanOrg
   [5]: http://www.tldp.org/
   [6]: http://www.linux.org/


