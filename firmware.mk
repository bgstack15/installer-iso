# This Makefile fragment defines how to set up the ${ISOTREE}/firmware
# directory

${warning adding links for all *firmware* and *microcode* in the pool}

FWPKGS = ${shell find ${ISOTREE}/pool -name '*firmware*.deb' -printf %P\ }
FWPKGS += ${shell find ${ISOTREE}/pool -name '*microcode*' -printf %P\ }

${ISOTREE}/firmware: ${ISOTREE}/firmware/
	ln -s ${addprefix ../pool/,${FWPKGS}} ${ISOTREE}/firmware/

