# Makefile fragment for preparing an isolinux boot into build/${ARCH}/isolinux
# and installing into
# an isolinux boot binary = ${ISOTREE}/boot/isolinux/isolinux.bin
# an isolinx boot config = ${ISOTREE}/boot/isolinux/isolinux.cfg
#
# Note: isolinux boot set up is the same for all ISO's within an architecture

# Relative path for isolinux on the target ISO = beside the kernel
ISOLINUXSYSDIR = ${patsubst %/,%,${dir ${KERNEL}}}
ISOLINUXDIR = ${ISOTREE}${ISOLINUXSYSDIR}

${warning ${ISOLINUXDIR}/}

# Relative path for the boot loader, used by xorriso
BOOTBIN = ${ISOLINUXSYSDIR}/isolinux.bin

# Where the sources are
ISOLINUXCFG = boot/isolinux

# Files for ${ISOLINUXDIR}
ISOLINUXFILES = isolinux.bin isolinux.cfg
ISOLINUXFILES += ${notdir ${wildcard ${ISOLINUXCFG}/*.cfg}}
ISOLINUXFILES += ${notdir ${wildcard ${ISOLINUXCFG}/*.txt}}
ISOLINUXFILES += vesamenu.c32 ldlinux.c32 libcom32.c32 libutil.c32

${ISOLINUXDIR}/%: /usr/lib/ISOLINUX/%
	@cp $< $@

${ISOLINUXDIR}/%: /usr/lib/syslinux/modules/bios/%
	@cp $< $@

# Copy boot/isolinux configuration while replacing variables:
ISOLINUXVARS = BEEP= BOOTPROMPT=install CONSOLE= 
ISOLINUXVARS += BUILD_DATE="${BUILD_DATE}" 
ISOLINUXVARS += DEBIAN_VERSION="${DISTNAME} ${DISTVERSION}"
ISOLINUXVARS += INITRD=${INITRD} KERNEL=${KERNEL}
ISOLINUXVARS += MEDIA_TYPE="ISO"
ISOLINUXVARS += SYSDIR=${ISOLINUXSYSDIR} SYSLINUX_CFG=isolinux.cfg
ISOLINUXVARS += VIDEO_MODE_GTK= VIDEO_MODE="vga=788 nomodeset" X86_KERNEL=

${ISOLINUXDIR}/isolinux.cfg: ${ISOLINUXCFG}/syslinux.cfg
	@env -i ${ISOLINUXVARS} envsubst < $< > $@

${ISOLINUXDIR}/%: ${ISOLINUXCFG}/%
	@env -i ${ISOLINUXVARS} envsubst < $< > $@

SPLASH = devuan-beowulf-bootscreen-iso-640x480.png
${ISOLINUXDIR}/splash.png: ${ISOLINUXCFG}/pics/${SPLASH}
	@cp $< $@

.PHONY: boot-bios-isolinux
boot-bios-isolinux: ${ISOLINUXDIR}/
boot-bios-isolinux: ${ISOLINUXDIR}/splash.png
boot-bios-isolinux: ${addprefix ${ISOLINUXDIR}/,${sort ${ISOLINUXFILES}}}
boot-bios-isolinux: ${ISOTREE}${INITRD} ${ISOTREE}${KERNEL}

BOOTOPTIONS += boot-bios-isolinux
